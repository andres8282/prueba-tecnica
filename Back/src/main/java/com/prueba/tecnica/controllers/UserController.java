package com.prueba.tecnica.controllers;

import com.prueba.tecnica.models.dto.RequestLoginDTO;
import com.prueba.tecnica.models.dto.ResponseDTO;
import com.prueba.tecnica.models.entity.User;
import com.prueba.tecnica.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Controller con los servicios asociados al usuario.
 */
@CrossOrigin(allowedHeaders = "*")
@RestController
@RequestMapping("/api/usuario")
public class UserController {

    @Autowired
    private IUserService userService;

    /**
     * Servicio para guardar un usuario en el sistema.
     */
    @PostMapping("/create")
    public ResponseDTO<User> createUser(@RequestBody User usuario) {
        return userService.createUser(usuario);
    }

    /**
     * Servicio para iniciar sesion.
     */
    @PostMapping("/login")
    public ResponseDTO<User> loginUser(@RequestBody RequestLoginDTO request) {
        return userService.loginUser(request);
    }
}
