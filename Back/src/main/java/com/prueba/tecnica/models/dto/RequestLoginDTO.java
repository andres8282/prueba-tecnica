package com.prueba.tecnica.models.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * DTO para request de login.
 */
@Data
@ToString
@NoArgsConstructor
public class RequestLoginDTO {
    private String userName;
    private String password;
}
